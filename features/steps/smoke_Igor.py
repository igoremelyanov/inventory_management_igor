from behave import *
from selenium import webdriver

@given('launch chrome browser')
def launchBrowser(context):
    #context.driver = webdriver.Chrome
    context.browser.get('http://www.google.com')
    #context.browser.get("https://bcvaxsit-citizenportal.cs148.force.com/s/")

@when('open client Portal page')
def openPortal(context):
    context.browser.get("https://bcvaxsit-citizenportal.cs148.force.com/s/")

@then('it should have a title "Home"')
def closeBrowser(context):
    assert context.browser.title == "Home"
    context.browser.get('http://www.google.com')


