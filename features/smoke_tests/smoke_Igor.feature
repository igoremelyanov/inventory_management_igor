Feature: Igor's Scripts

  Scenario: Visit client's Portal booking page
    Given launch chrome browser
    When open client Portal page
    Then it should have a title "Home"

 #Scenario: Citizen Igor77777777 Portal Flow SIT
    #Given user is on Citizen portal HOME page as SIT
    #When the user Register and books an appointment using the portal in SIT.

  #Scenario: Clinician Igor77777 Flow Dose 2 SIT
    #Given user is on clinician_sit Login Page.
    #When the user provide the clinician_sit username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen in SIT environment.
    ###Then the user Register and books an appointment using the clinician in SIT for API testing purpose.
    #Then the user Register and do API testing through clinician in SIT.