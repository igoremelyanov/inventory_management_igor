from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait

from .common.basepage import BASEPAGE
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from utils.general import *
import time


class RegisterUser(BASEPAGE):

    locator_dictionary = {
        "title_of_page": (By.XPATH, './/div[text() = "Register to get vaccinated"]'),
        "register_btn": (By.XPATH, './/button/span[text() = "Register now"]'),
        "first_name": (By.XPATH, '//input[@name="FirstName"]'),
        "last_name": (By.XPATH, '//input[@name="LastName"]'),
        "dob": (By.XPATH, '//input[@name="PersonBirthdate"]'),
        "postal_code": (By.XPATH, '//input[@name="DDH_HC_Zip_Code"]'),
        "phn_number": (By.XPATH, '//input[@name="HC_Personal_Health_Number"]'),
        "continue_btn": (By.XPATH, './/button[text() = "Continue"]'),
        "all_issues":(By.XPATH, './/*[@class = "slds-form-element__help"]'),
        #"email_checkbox": (By.XPATH, '//*[@id="email-27"]'),
        "email_radiobutton":(By.XPATH,'//input[@value="Email"]'),
        "email_textbox":(By.XPATH,'//input[@name="PersonEmail"]'),
        "email_address": (By.XPATH, '//*[@id="input-28"]'),
        "confirm_email_address": (By.XPATH, '//*[@name = "ConfirmEmail"]'),
        "continue_btn_2": (By.XPATH, '(.//*/button[text() = "Continue"])[2]'),
        "patient_consent_checkbox": (By.XPATH, '//input[@name="DDH_HC_Patient_Consent"]'),
        "submit_btn": (By.XPATH, './/button[text() = "Submit"]'),
        "reg_number": (By.XPATH, './/div[@class = "confirmation-number"]')
    }
    constants = {
       # "link": '/s/'
        "register_user": '/s/'
    }

    def go_to(self, link):
        #base_url = get_setting("URL", "portal_url")
        #self.browser.get(base_url + self.constants["link"])
        # self.browser.get(base_url)
        if (link == "SIT"):
            base_url = get_setting("URL", "portal_url_sit")
            supply_console_url = self.constants["register_user"]
            self.browser.get(base_url + supply_console_url)
        if (link == "UAT"):
            base_url = get_setting("URL", "portal_url_uat")
            supply_console_url = self.constants["register_user"]
            self.browser.get(base_url + supply_console_url)
        if (link == "PRODSUPPQA"):
            base_url = get_setting("URL", "portal_url_prodsuppqa")
            supply_console_url = self.constants["register_user"]
            self.browser.get(base_url + supply_console_url)
        if (link == "PERF"):
            base_url = get_setting("URL", "portal_url_perf")
            supply_console_url = self.constants["register_user"]
            self.browser.get(base_url + supply_console_url)
        try:
            WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["title_of_page"]))
        except:
            print("Exception: The user is not navigated to Portal Home screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["title_of_page"])
        assert var is not None, "The user is not navigated to Portal Home screen."

    def click_register_btn(self, register_btn):
        # time.sleep(2)

        e = ""
        try:
            WebDriverWait(self.browser, self.WAIT).until(
                EC.element_to_be_clickable(self.locator_dictionary["register_btn"])).click()
        except:
            print("Exception: The user is not navigated to Portal Home screen.")
        # self.click_element(self.find_element(self.locator_dictionary["register_btn"]))
        try:
            e = WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["continue_btn"]))
        except:
            print("The form is not appeared.")
        assert e is not None, "The form is not appeared."

    def fill_form_step_one(self, first_name, last_name, dob, postal_code, phn_number):
        e = None
        self.send_text_to_element(self.find_element(self.locator_dictionary["first_name"]), first_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["last_name"]), last_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["dob"]), dob)
        self.send_text_to_element(self.find_element(self.locator_dictionary["postal_code"]), postal_code)
        self.send_text_to_element(self.find_element(self.locator_dictionary["phn_number"]), phn_number)

        self.click_element(self.find_element(self.locator_dictionary["continue_btn"]))
        time.sleep(1)
        try:
            e = WebDriverWait(self.browser, 2).until(
                EC.visibility_of_element_located(self.locator_dictionary["email_textbox"]))
        except:
            print("Exception: The form 2 is not appeared.")
            all_issues = self.find_elements(self.locator_dictionary["all_issues"])
            # print(all_issues.to_list())
            issue_msg = "Registration Issues!\n"
            i = 1
            for issue in all_issues:
                text_of_issue = str(i) + ") " + self.get_element_text(issue)
                issue_msg = issue_msg + text_of_issue + "\n"
                i = i + 1
            issue_msg = issue_msg.rstrip("\n")
            return issue_msg

        assert e is not None, "The form step 2 is not appeared."
        return ""

    def fill_form_step_two(self, email,env_name):
        e = ""
        #self.click_element(self.find_element(self.locator_dictionary["email_checkbox"]))
        #print(email)
        self.send_text_to_element(self.find_element(self.locator_dictionary["email_textbox"]), email)
        #if(env_name=="PRODSUPPQA"):
            #self.click_element(self.find_element(self.locator_dictionary["email_radiobutton"]))
        self.send_text_to_element(self.find_element(self.locator_dictionary["confirm_email_address"]), email)

        self.click_element(self.find_element(self.locator_dictionary["continue_btn_2"]))
        try:
            e = WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["submit_btn"]))
        except:
            print("The form is not appeared.")
        assert e is not None, "The form step 3 is not appeared."

    def submit_form(self, consent_btn, submit_btn):
        e = ""
        time.sleep(2)
        self.click_element(self.find_element(self.locator_dictionary["patient_consent_checkbox"]))
        time.sleep(1)
        self.click_element(self.find_element(self.locator_dictionary["submit_btn"]))
        try:
            e = WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["reg_number"]))
        except:
            print("The form was not submitted. Try Again!")
        time.sleep(2)
        assert e is not None, "The form was not submitted. Try Again!"

    def refresh_browser(self, df):
        # df.to_csv('data.csv', index=False)
        base_url = get_setting("URL", "portal_url")
        self.browser.get(base_url + self.constants["link"])

    def save_reg_number(self, reg_number):
        r = self.get_element_text(self.find_element(self.locator_dictionary["reg_number"]))
        r = r.replace(" ", "")
        print(r)
        return r

