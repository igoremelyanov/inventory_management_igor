from datetime import datetime

from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait

from .common import RegApi
from .common.basepage import BASEPAGE
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from utils.general import *
import time


class ClinicalPrecess(BASEPAGE):
    locator_dictionary = {
        "nav_button": (By.XPATH, './/div[@role="navigation"]/one-app-launcher-header/button'),
        "home_nav_button": (By.XPATH, './/a[@title="Home"]'),
        "nav_search_bar": (By.XPATH, '(.//input[@type="search"])[2]'),
        "hospital_field": (By.XPATH, '(.//div[@class="slds-form slds-m-top_large"]//input)[3]'),
        "hospital_field_cancel_button": (By.XPATH, './/div[@class="slds-form slds-m-top_large"]//button[@title="Clear Selection"]'),
        "reg_number_field": (By.XPATH, '(.//div[@class="slds-form slds-m-top_large"]//input)[2]'),
        "search_btn": (By.XPATH , './/button[text() = "Search"]'),
        "view_btn": (By.XPATH, './/button[@title="View Case"]'),
        "current_step": (By.XPATH, './/div[contains(@class, "current-step")]/h2'),
        "edit_icon": (By.XPATH, './/span[text()="Identification Information"]//parent::span//following-sibling::lightning-button//button[@title="Edit"]'),

        "address_field": (By.XPATH, './/input[@name="DDH__HC_Address_1__c"]'),
        "city_field": (By.XPATH, './/input[@name="DDH__HC_City__c"]'),
        "state_field": (By.XPATH, './/input[@name="DDH__HC_State__c"]'),
        "zip_code_field": (By.XPATH, './/input[@name="DDH__HC_Zip_Code__c"]'),
        "save_btn": (By.XPATH, './/button[text() = "Save"]'),
        "save_btn_vaccine": (By.XPATH, './/button[text() = "Save"][2]'),
        "return_to_search_btn": (By.XPATH, './/button[text() = "Return to Search"]'),
        "edit_success": (By.XPATH, './/div[text() = "Information updated"]'),
        "rebook_btn": (By.XPATH, './/button[text() = "Rebook at Current Location"]'),
        "rebook_success": (By.XPATH, './/span[text() = "Appointment is successfully booked"]'),
        "confirm_btn_1": (By.XPATH, './/button[@class="save-button"]'), # information_updated, current_step
        "clinician_name": (By.XPATH, './/label[text() = "Informed Consent Provider (User)"]/parent::lightning-grouped-combobox//input[@placeholder="Clinician Automation"]'),
        "return_btn": (By.XPATH, './/button[text() = "Return to Search"]'),

        "related_tab_id": (By.XPATH, './/a[@id = "relatedListsTab__item"]'),
        "immunization_no": (By.XPATH, './/lightning-formatted-url/a[@target="_self"]'),
        "record_status": (By.XPATH, './/span[text() = "Pathway Status"]/parent::div/parent::div//lightning-formatted-text[@data-output-element-id="output-field"]'),
        "user_defaults_page": (By.XPATH, './/a[@title = "User Defaults"]'),
        "default_date_field": (By.XPATH, './/input[@name="BCH_Date__c"]'),
        "default_success_toast": (By.XPATH, './/span[text() = "User defaults successfully updated."]'),
        "check_in_button": (By.XPATH, './/button[@title="Check-in Client"]'),
        "is_checked_in": (By.XPATH, ''),
        "related_tab_new_registered": (By.XPATH, '//li[@data-label="Related"]'),
        "immunization_record_link": (By.XPATH, '//th[@data-label="Immunization Record"]//a'),
        "case_number": (By.XPATH, '//h1//div[text()="Immunization Record"]//following-sibling::slot//lightning-formatted-text')
    }

    constants = {
        "in_clinic": '/lightning/n/BCH_In_Clinic_Experience_Home',
        "clinic_in_box": '/lightning/page/home',
    }

    def go_to(self):
        base_url = get_setting("URL", "sf_org")
        self.browser.get(base_url + self.constants["link"])
        # self.browser.get(base_url)
        try:
            WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["login_btn"]))
        except:
            print("Exception: The user is not navigated to Login screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["login_btn"])
        assert var is not None, "The user is not navigated to Login screen."

    def login_into_website(self, user_name, password, home_screen):
        self.send_text_to_element(self.find_element(self.locator_dictionary["user_name"]), user_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["password"]), password)

        self.click_element(self.find_element(self.locator_dictionary["login_btn"]))
        try:
            WebDriverWait(self.browser, 60).until(
                EC.presence_of_element_located(self.locator_dictionary["register_btn"]))
        except:
            print("Exception: The user is not navigated to Home screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["register_btn"])
        assert var is not None, "The user is not navigated to Home screen."

    def move_to(self, module_name):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["nav_button"]))
        self.send_text_to_element(self.find_element(self.locator_dictionary["nav_search_bar"]), module_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["nav_search_bar"]), Keys.ENTER)
        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["reg_number_field"]))
        except:
            print("Exception: The user is not navigated to" + module_name + " screen.")
        assert e is not None, "The user is not navigated to " + module_name + "screen."

    def navigate_to_home(self):
        e = None
        # baseURL = get_setting("URL", "sf_org")
        # self.browser.get(baseURL + self.constants["in_clinic"])
        self.browser.execute_script("arguments[0].click();",
                                    self.find_element(self.locator_dictionary["home_nav_button"]))
        # self.click_element(self.find_element(self.locator_dictionary["home_nav_button"]))
        # self.send_text_to_element(self.find_element(self.locator_dictionary["nav_search_bar"]), module_name)
        # self.send_text_to_element(self.find_element(self.locator_dictionary["nav_search_bar"]), Keys.ENTER)
        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["reg_number_field"]))
        except:
            print("Exception: The user is not navigated to Home screen.")
        assert e is not None, "The user is not navigated to Home screen."

    def search_citizen(self, reg_no):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["hospital_field_cancel_button"]))
        self.send_text_to_element(self.find_element(self.locator_dictionary["reg_number_field"]), reg_no)
        self.click_element(self.find_element(self.locator_dictionary["search_btn"]))

        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["view_btn"]))
        except:
            print("Exception")
        assert e is not None, "The searched records are not present."

    def view_record(self, process_screen):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["view_btn"]))
        time.sleep(2)
        loc = './/div[contains(@class, "current-step")]/h2[text() = \"' + process_screen + '\"]'
        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located((By.XPATH, loc)))
        except:
            print("Exception")
        #print(self.get_element_text(e))
        assert self.get_element_text(e) == process_screen, "The " + process_screen + " process is in wrong stage."

    def edit_record(self):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["edit_icon"]))

        e = WebDriverWait(self.browser, 20).until(
            EC.presence_of_element_located(self.locator_dictionary["address_field"]))


        address_field = self.get_attribute(self.find_element(self.locator_dictionary["address_field"]), "value")
        if address_field == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["address_field"]), "8801 SECOND AVE")

        city_field = self.get_attribute(self.find_element(self.locator_dictionary["city_field"]), "value")
        if city_field == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["city_field"]), "VANCOUVER")

        state_field = self.get_attribute(self.find_element(self.locator_dictionary["state_field"]), "value")
        if state_field == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["state_field"]), "BC")

        zip_code_field = self.get_attribute(self.find_element(self.locator_dictionary["zip_code_field"]), "value")
        if zip_code_field == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["zip_code_field"]), "V0R2Y0")

        # self.click_element(self.find_element(self.locator_dictionary["save_btn"]))
        time.sleep(2)
        self.browser.execute_script("arguments[0].click();",
                                    self.find_element(self.locator_dictionary["save_btn"]))
        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["edit_success"]))
        except:
            print("Exception")
        assert e is not None, "The fields are not updated."

    def save_data(self):
        done = False
        WebDriverWait(self.browser, 20).until(EC.visibility_of_element_located(self.locator_dictionary["clinician_name"]))
        self.click_element(self.find_element(self.locator_dictionary["save_btn"]))
        # while not done:
        #     self.click_element(self.find_element(self.locator_dictionary["save_btn"]))
        #     time.sleep(2)
        #     elem = None
        #     try:
        #         elem = self.find_element(self.locator_dictionary["save_btn"])
        #         done = True
        #     except:
        #         if elem is None:
        #             done = True
        #             print("broken")
        #         else:
        #             continue

    def rebook_appointment(self, rebook_btn):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["rebook_btn"]))

        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["rebook_success"]))
        except:
            assert "Exception: The appointment re-booking failed."
        # assert e is not None, "The appointment re-booking failed."

    def confirm_step(self):
        e = None
        time.sleep(5)
        self.click_element(self.find_element(self.locator_dictionary["confirm_btn_1"]))
        # self.browser.execute_script("arguments[0].click();",
        #                             self.find_element(self.locator_dictionary["confirm_btn_1"]))
        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["edit_success"]))
        except:
            print("Exception")
        assert e is not None, "The appointment confirmation failed."

    def go_to_patient_record_details(self, citizen_record):
        self.browser.get(citizen_record)
        try:
            WebDriverWait(self.browser, 20).until(
                EC.visibility_of_element_located(self.locator_dictionary["related_tab_id"]))
        except:
            assert False, "Citizen record in not appeared."

        time.sleep(2)
        e = self.find_element(self.locator_dictionary["related_tab_id"])
        immunization_no = ""
        try:
            e.click()
            immunization_no = WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["immunization_no"]))
        except:
            print("Exception: The user is not navigated to citizen Detail screen.")
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["related_tab_id"]))
            immunization_no = WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["immunization_no"]))
        assert immunization_no is not None, "The user is not navigated to citizen Detail screen."
        return self.get_element_text(immunization_no)

    def open_immunization_record(self):
        elem = None
        time.sleep(2)
        e = self.find_element(self.locator_dictionary["immunization_no"])
        immunization_no = ""
        try:
            e.click()
            time.sleep(5)
            elem = WebDriverWait(self.browser, self.WAIT).until(
                EC.visibility_of_element_located(self.locator_dictionary["record_status"]))
        except:
            #print("Exception: The user is not navigated to citizen Detail screen.")
            self.browser.execute_script("arguments[0].click();",
                                        self.find_element(self.locator_dictionary["immunization_no"]))
            time.sleep(5)
            elem = WebDriverWait(self.browser, self.WAIT).until(
                EC.visibility_of_element_located(self.locator_dictionary["record_status"]))

        #print(self.get_element_text(elem))
        assert self.get_element_text(elem) == "After Care", "The record is not yet in clinic."
        time.sleep(5)

    def set_user_default(self):
        date_field = datetime.today().strftime('%b %d, %Y')
        # date_field = "May 25, 2021"
        self.browser.execute_script("arguments[0].click();",
                                    self.find_element(self.locator_dictionary["user_defaults_page"]))
        # self.click_element(self.find_element(self.locator_dictionary["user_defaults_page"]))
        default_date_field = WebDriverWait(self.browser, 30).until(EC.visibility_of_element_located(self.locator_dictionary["default_date_field"]))
        time.sleep(2)
        default_date_field.clear()
        default_date_field.send_keys(date_field)
        self.click_element(self.find_element(self.locator_dictionary["save_btn"]))
        default_date_field = WebDriverWait(self.browser, 30).until(EC.visibility_of_element_located(self.locator_dictionary["default_success_toast"]))
        assert default_date_field is not None, "The user defaults not saved."
        return True

    def check_in_patient(self):
        self.browser.execute_script("arguments[0].click();",
                                    self.find_element(self.locator_dictionary["check_in_button"]))
        temp = ""
        try:
            WebDriverWait(self.browser, 30).until(
                EC.presence_of_element_located(self.locator_dictionary["rebook_btn"]))
            temp = "In Clinic"
        except:
            print("Exception!")
            temp = "Ineligible"
        time.sleep(5)
        return temp

    def save_appointment_no(self, reg,env_name):
        ap_no = RegApi.get_booking_no(reg,env_name)
        return ap_no

    def get_pathway_status(self):
        self.click_element(self.find_element(self.locator_dictionary["related_tab_new_registered"]))
        immunization_id=self.find_element((self.locator_dictionary["immunization_record_link"])).text
        #print(immunization_id)
        #self.click_element(self.find_element(self.locator_dictionary["immunization_record_link"]))
        #case_number=self.get_attribute(self.find_element(self.locator_dictionary["case_number"]), "value")
        #print("case number")
        #print(case_number)
        return immunization_id

#         300042698 - Exp. 2021 March 19
# name="lotNum"
