from testrail import *

def testrail_success(flow_name,terminal_output):
    client = APIClient('https://qmsoftwaretest.testrail.io/')
    client.user = 'ngabriel@deloitte.ca'
    client.password = 'Bcvaxtestrail07*'
    if(flow_name=="Citizen Portal Flow PRODSUPPQA"):
        result = client.send_post(
         'add_result_for_case/2842/119446',
        {'status_id': 1, 'comment': terminal_output})
    elif(flow_name=="Call Center Agent Flow PRODSUPPQA"):
        result = client.send_post(
            'add_result_for_case/2842/119445',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Clinician Flow PRODSUPPQA"):
        result = client.send_post(
            'add_result_for_case/2842/119459',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow PRODSUPPQA"):
        result = client.send_post(
            'add_result_for_case/2842/121756',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Citizen Portal Flow SIT"):
        result = client.send_post(
            'add_result_for_case/3233/142807',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Call Center Agent Flow SIT"):
        result = client.send_post(
            'add_result_for_case/3233/142806',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Clinician Flow SIT"):
        result = client.send_post(
            'add_result_for_case/3233/142808',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow SIT"):
        result = client.send_post(
            'add_result_for_case/3233/142814',
            {'status_id': 1, 'comment': terminal_output})
        result = client.send_post(
            'add_result_for_case/3233/142815',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Citizen Portal Flow UAT"):
        result = client.send_post(
            'add_result_for_case/3234/142807',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Call Center Agent Flow UAT"):
        result = client.send_post(
            'add_result_for_case/3234/142806',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Clinician Flow UAT"):
        result = client.send_post(
            'add_result_for_case/3234/142808',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow UAT"):
        result = client.send_post(
            'add_result_for_case/3234/142814',
            {'status_id': 1, 'comment': terminal_output})
        result = client.send_post(
            'add_result_for_case/3234/142815',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Citizen Portal Flow PERF"):
        result = client.send_post(
            'add_result_for_case/3073/119446',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Call Center Agent Flow PERF"):
        result = client.send_post(
            'add_result_for_case/3073/119445',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Clinician Flow PERF"):
        result = client.send_post(
            'add_result_for_case/3073/119459',
            {'status_id': 1, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow PERF"):
        result = client.send_post(
            'add_result_for_case/3073/137382',
            {'status_id': 1, 'comment': terminal_output})
        result = client.send_post(
            'add_result_for_case/3073/137383',
            {'status_id': 1, 'comment': terminal_output})

def testrail_fail(flow_name,terminal_output):
    client = APIClient('https://qmsoftwaretest.testrail.io/')
    client.user = 'ngabriel@deloitte.ca'
    client.password = 'Bcvaxtestrail07*'
    # if (flow_name == "InventoryFlow"):
    #     result = client.send_post(
    #         'add_result_for_case/2746/121756',
    #         {'status_id': 5, 'comment': 'The transfer between the clinics were not successful. Reason : Remaining quantities are not updated properly.You will need to change either the agent on the supply item or the agent on the trade to make sure that they are matching'})
    if (flow_name == "Citizen Portal Flow PRODSUPPQA"):
        result = client.send_post(
            'add_result_for_case/2842/119446',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Call Center Agent Flow PRODSUPPQA"):
        result = client.send_post(
            'add_result_for_case/2842/119445',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Clinician Flow PRODSUPPQA"):
        result = client.send_post(
            'add_result_for_case/2842/119459',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow PRODSUPPQA"):
        result = client.send_post(
            'add_result_for_case/2842/121756',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Citizen Portal Flow SIT"):
        result = client.send_post(
            'add_result_for_case/3233/142807',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Call Center Agent Flow SIT"):
        result = client.send_post(
            'add_result_for_case/3233/142806',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Clinician Flow SIT"):
        result = client.send_post(
            'add_result_for_case/3233/142808',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow SIT"):
        result = client.send_post(
            'add_result_for_case/3233/142814',
            {'status_id': 5, 'comment': terminal_output})
        result = client.send_post(
            'add_result_for_case/3233/142815',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Citizen Portal Flow UAT"):
        result = client.send_post(
            'add_result_for_case/3234/142807',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Call Center Agent Flow UAT"):
        result = client.send_post(
            'add_result_for_case/3234/142806',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Clinician Flow UAT"):
        result = client.send_post(
            'add_result_for_case/3234/142808',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow UAT"):
        result = client.send_post(
            'add_result_for_case/3234/142814',
            {'status_id': 5, 'comment': terminal_output})
        result = client.send_post(
            'add_result_for_case/3234/142815',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow SIT Failed"):
        result = client.send_post(
            'add_result_for_case/2843/142814',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Citizen Portal Flow PERF"):
        result = client.send_post(
            'add_result_for_case/3073/119446',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Call Center Agent Flow PERF"):
        result = client.send_post(
            'add_result_for_case/3073/119445',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Clinician Flow PERF"):
        result = client.send_post(
            'add_result_for_case/3073/119459',
            {'status_id': 5, 'comment': terminal_output})
    elif (flow_name == "Inventory Supply Flow PERF"):
        result = client.send_post(
            'add_result_for_case/3073/137382',
            {'status_id': 5, 'comment': terminal_output})
        result = client.send_post(
            'add_result_for_case/3073/137383',
            {'status_id': 5, 'comment': terminal_output})
