from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait

from features.environment import before_scenario
from utils.browsers import browsers
from .common.basepage import BASEPAGE
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from utils.general import *
import time
import os
from utils.paths import project_path


class Login(BASEPAGE):
    locator_dictionary = {
        "user_name": (By.ID, 'username'),
        "password": (By.ID, 'password'),
        "login_btn": (By.ID, 'Login'),
        "register_btn": (By.XPATH, './/button[@title = " Create New Profile"]'),
        "close_btns": (By.XPATH, './/button[@title="Close TEST PANOSHARPIE"]')
    }

    constants = {
        "call_center_agent": '/lightning/page/home/',
        "clinician": '/lightning/n/BCH_Register',
        "pphis": '/lightning/o/HC_Supply_Location__c/list'
    }

    # def go_to(self, link):
    #     base_url = get_setting("URL", "sf_org")
    #     self.browser.get(base_url + self.constants[link])
    #     # self.browser.get(base_url)
    #     try:
    #         WebDriverWait(self.browser, self.WAIT).until(
    #             EC.presence_of_element_located(self.locator_dictionary["login_btn"]))
    #     except:
    #         print("Exception: The user is not navigated to Login screen.")
    #     # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
    #     var = self.find_element(self.locator_dictionary["login_btn"])
    #     assert var is not None, "The user is not navigated to Login screen."

    def go_to(self, link):
        print(link)
        if (link == "clinician_sit"):
            base_url = get_setting("URL", "sf_org_sit")
            supply_console_url = self.constants["clinician"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "clinician_uat"):
            base_url = get_setting("URL", "sf_org_uat")
            supply_console_url = self.constants["clinician"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "clinician_prodsuppqa"):
            base_url = get_setting("URL", "sf_org_prodsuppqa")
            supply_console_url = self.constants["clinician"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "clinician_perf"):
            base_url = get_setting("URL", "sf_org_perf")
            supply_console_url = self.constants["clinician"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "call_center_agent_sit"):
            base_url = get_setting("URL", "sf_org_sit")
            supply_console_url = self.constants["call_center_agent"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "call_center_agent_uat"):
            base_url = get_setting("URL", "sf_org_uat")
            supply_console_url = self.constants["call_center_agent"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "call_center_agent_prodsuppqa"):
            base_url = get_setting("URL", "sf_org_prodsuppqa")
            supply_console_url = self.constants["call_center_agent"]
            self.browser.get(base_url + supply_console_url)
        elif (link == "call_center_agent_perf"):
            base_url = get_setting("URL", "sf_org_perf")
            supply_console_url = self.constants["call_center_agent"]
            self.browser.get(base_url + supply_console_url)
        # self.browser.get(base_url)
        try:
            WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["login_btn"]))
        except:
            print("Exception: The user is not navigated to Login screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["login_btn"])
        assert var is not None, "The user is not navigated to Login screen."

    def login_into_website(self, user_name, password, home_screen,env_name):
        self.send_text_to_element(self.find_element(self.locator_dictionary["user_name"]), user_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["password"]), password)

        self.click_element(self.find_element(self.locator_dictionary["login_btn"]))
        self.is_log_in("no",env_name)

    # def login_into_website(self, user_name, password, home_screen):
    #     self.send_text_to_element(self.find_element(self.locator_dictionary["user_name"]), user_name)
    #     self.send_text_to_element(self.find_element(self.locator_dictionary["password"]), password)
    #
    #     self.click_element(self.find_element(self.locator_dictionary["login_btn"]))
    #     self.is_log_in("no")

    def is_log_in(self, already_log_in,env_name):
        if already_log_in != "no":
            if(env_name=="SIT"):
                base_url = get_setting("URL", "sf_org_sit")
                self.browser.get(base_url + self.constants[already_log_in])
            elif(env_name=="UAT"):
                base_url = get_setting("URL", "sf_org_uat")
                self.browser.get(base_url + self.constants[already_log_in])
            elif(env_name=="PRODSUPPQA"):
                base_url = get_setting("URL", "sf_org_prodsuppqa")
                self.browser.get(base_url + self.constants[already_log_in])
            elif (env_name == "PERF"):
                base_url = get_setting("URL", "sf_org_perf")
                self.browser.get(base_url + self.constants[already_log_in])
        try:
            WebDriverWait(self.browser, 60).until(
                EC.presence_of_element_located(self.locator_dictionary["register_btn"]))
        except:
            print("Exception: The user is not navigated to Home screen.")

        # close_btns = self.find_elements(self.locator_dictionary["close_btns"])
        # print("Total opened tab: " + str(len(close_btns)))
        # for button in close_btns:
        #     self.click_element(button)
        #     time.sleep(3)
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["register_btn"])
        assert var is not None, "The user is not navigated to Home screen."

    def close_browser(self, user_type):
        if user_type == "call_center_agent":
            self.browser.quit()
            default_browser = get_browser_name()
            self.browser = browsers(self, get_config().userdata.get("browser", default_browser))